from pyray import *


# --------------------------------------------------------------------------------------
# Module functions definition
# --------------------------------------------------------------------------------------

# Draw text using font inside rectangle limits
def DrawTextBoxed(font: Font, text: str, rec: Rectangle, fontSize: float, spacing: float, wordWrap: bool, tint: Color):
    DrawTextBoxedSelectable(font, text, rec, fontSize, spacing, wordWrap, tint, 0, 0, WHITE, WHITE)


# Draw text using font inside rectangle limits with support for text selection
def DrawTextBoxedSelectable(font: Font, text: str, rec: Rectangle, fontSize: float, spacing: float, wordWrap: bool,
                            tint: Color, selectStart: int, selectLength: int, selectTint: Color, selectBackTint: Color):
    length: int = text_length(text)  # Total length in bytes of the text, scanned by codepoints in loop

    textOffsetY: float = 0  # Offset between lines (on line break '\n')
    textOffsetX: float = 0.0  # Offset X to next character to draw

    scaleFactor: float = fontSize / font.baseSize  # Character rectangle scaling factor

    # Word/character wrapping mechanism variables
    MEASURE_STATE = 0
    DRAW_STATE = 1
    state: int = MEASURE_STATE if wordWrap else DRAW_STATE

    startLine: int = -1  # Index where to begin drawing (where a line begins)
    endLine: int = -1  # Index where to stop drawing (where a line ends)
    lastk: int = -1  # Holds last value of the character position

    k = -1
    for i in range(0, length):
        k += 1
        # Get next codepoint from byte string and glyph index in font
        codepointByteCount: int = 0
        codepoint: int = get_codepoint(text[i], codepointByteCount)
        index: int = get_glyph_index(font, codepoint)

        # NOTE: Normally we exit the decoding sequence as soon as a bad byte is found (and return 0x3f)
        # but we need to draw all of the bad bytes using the '?' symbol moving one byte
        if codepoint == 0x3f:
            codepointByteCount = 1
        i += (codepointByteCount - 1)

        glyphWidth: float = 0
        if codepoint != '\n':
            glyphWidth = font.recs[index].width * scaleFactor if (font.glyphs[index].advanceX == 0) else font.glyphs[
                                                                                                             index].advanceX * scaleFactor

            if i + 1 < length:
                glyphWidth = glyphWidth + spacing

        # NOTE: When wordWrap is ON we first measure how much of the text we can draw before going outside of the rec container
        # We store this info in startLine and endLine, then we change states, draw the text between those two variables
        # and change states again and again recursively until the end of the text (or until we get outside of the container).
        # When wordWrap is OFF we don't need the measure state so we go to the drawing state immediately
        # and begin drawing on the next line before we can get outside the container.
        if state == MEASURE_STATE:
            # TODO: There are multiple types of spaces in UNICODE, maybe it's a good idea to add support for more
            # Ref: http:#jkorpela.fi/chars/spaces.html
            if (codepoint == ' ') or (codepoint == '\t') or (codepoint == '\n'):
                endLine = i

            if (textOffsetX + glyphWidth) > rec.width:
                endLine = i if (endLine < 1) else endLine
                if i == endLine:
                    endLine -= codepointByteCount
                if (startLine + codepointByteCount) == endLine:
                    endLine = (i - codepointByteCount)

                state = not state
            elif (i + 1) == length:
                endLine = i
                state = not state
            elif codepoint == '\n':
                state = not state

            if state == DRAW_STATE:
                textOffsetX = 0
                i = startLine
                glyphWidth = 0

                # Save character position when we switch states
                tmp: int = lastk
                lastk = k - 1
                k = tmp

        else:
            if codepoint == '\n':
                if not wordWrap:
                    textOffsetY += (font.baseSize + font.baseSize / 2) * scaleFactor
                    textOffsetX = 0
            else:
                if not wordWrap and ((textOffsetX + glyphWidth) > rec.width):
                    textOffsetY += (font.baseSize + font.baseSize / 2) * scaleFactor
                    textOffsetX = 0

                # When text overflows rectangle height limit, just stop drawing
                if (textOffsetY + font.baseSize * scaleFactor) > rec.height:
                    break

                # Draw selection background
                isGlyphSelected: bool = False
                if (selectStart >= 0) and (k >= selectStart) and (k < (selectStart + selectLength)):
                    draw_rectangle_rec(Rectangle(rec.x + textOffsetX - 1, rec.y + textOffsetY, glyphWidth,
                                                 font.baseSize * scaleFactor), selectBackTint)
                    isGlyphSelected = True

                # Draw current character glyph
                if (codepoint != ' ') and (codepoint != '\t'):
                    draw_text_codepoint(font, codepoint, Vector2(rec.x + textOffsetX, rec.y + textOffsetY), fontSize,
                                        selectTint if isGlyphSelected else tint)

            if wordWrap and (i == endLine):
                textOffsetY += (font.baseSize + font.baseSize / 2) * scaleFactor
                textOffsetX = 0
                startLine = endLine
                endLine = -1
                glyphWidth = 0
                selectStart += lastk - k
                k = lastk

                state = not state

        if (textOffsetX != 0) or (codepoint != ' '):
            textOffsetX += glyphWidth  # avoid leading spaces


# ------------------------------------------------------------------------------------
# Program main entry point
# ------------------------------------------------------------------------------------

# Initialization
# --------------------------------------------------------------------------------------
screenWidth: int = 800
screenHeight: int = 450

init_window(screenWidth, screenHeight, "raylib [text] example - draw text inside a rectangle")

text = "Text cannot escape\tthis container\t...word wrap also works when active so here's \
a long text for testing.\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod \
tempor incididunt ut labore et dolore magna aliqua. Nec ullamcorper sit amet risus nullam eget felis eget."

resizing: bool = False
wordWrap: bool = True

container: Rectangle = Rectangle(25.0, 25.0, screenWidth - 50.0, screenHeight - 250.0)
resizer: Rectangle = Rectangle(container.x + container.width - 17, container.y + container.height - 17, 14, 14)

# Minimum width and heigh for the container rectangle
minWidth: float = 60
minHeight: float = 60
maxWidth: float = screenWidth - 50.0
maxHeight: float = screenHeight - 160.0

lastMouse: Vector2 = Vector2(0.0, 0.0)  # Stores last mouse coordinates
borderColor: Color = MAROON  # Container border color
font: Font = get_font_default()  # Get default system font

set_target_fps(60)  # Set our game to run at 60 frames-per-second
# --------------------------------------------------------------------------------------

# Main game loop
while not window_should_close():  # Detect window close button or ESC key

    # Update
    # ----------------------------------------------------------------------------------
    if is_key_pressed(KeyboardKey.KEY_SPACE):
        wordWrap = not wordWrap

    mouse: Vector2 = get_mouse_position()

    # Check if the mouse is inside the container and toggle border color
    if check_collision_point_rec(mouse, container):
        borderColor = fade(MAROON, 0.4)
    elif not resizing:
        borderColor = MAROON

    # Container resizing logic
    if resizing:
        if is_mouse_button_released(MouseButton.MOUSE_BUTTON_LEFT):
            resizing = False

        width: float = container.width + (mouse.x - lastMouse.x)
        container.width = (width if (width < maxWidth) else maxWidth) if (width > minWidth) else minWidth

        height: float = container.height + (mouse.y - lastMouse.y)
        container.height = (height if (height < maxHeight) else maxHeight) if (height > minHeight) else minHeight
    else:
        # Check if we're resizing
        if is_mouse_button_down(MouseButton.MOUSE_BUTTON_LEFT) and check_collision_point_rec(mouse, resizer):
            resizing = True

    # Move resizer rectangle properly
    resizer.x = container.x + container.width - 17
    resizer.y = container.y + container.height - 17

    lastMouse = mouse  # Update mouse
    # ----------------------------------------------------------------------------------

    # Draw
    # ----------------------------------------------------------------------------------
    begin_drawing()

    clear_background(RAYWHITE)

    draw_rectangle_lines_ex(container, 3, borderColor)  # Draw container border

    # Draw text in container (add some padding)
    DrawTextBoxed(font, text, Rectangle(container.x + 4, container.y + 4, container.width - 4, container.height - 4),
                  20.0, 2.0, wordWrap, GRAY)

    draw_rectangle_rec(resizer, borderColor)  # Draw the resize box

    # Draw bottom info
    draw_rectangle(0, screenHeight - 54, screenWidth, 54, GRAY)
    draw_rectangle_rec(Rectangle(382.0, screenHeight - 34.0, 12.0, 12.0), MAROON)

    draw_text("Word Wrap: ", 313, screenHeight - 115, 20, BLACK)
    if wordWrap:
        draw_text("ON", 447, screenHeight - 115, 20, RED)
    else:
        draw_text("OFF", 447, screenHeight - 115, 20, BLACK)

    draw_text("Press [SPACE] to toggle word wrap", 218, screenHeight - 86, 20, GRAY)

    draw_text("Click hold & drag the    to resize the container", 155, screenHeight - 38, 20, RAYWHITE)

    end_drawing()
    # ----------------------------------------------------------------------------------

# De-Initialization
# --------------------------------------------------------------------------------------
close_window()  # Close window and OpenGL context
# --------------------------------------------------------------------------------------
