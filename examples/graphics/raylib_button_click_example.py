#
# Adapted from: https://github.com/raysan5/raylib/blob/master/examples/textures/textures_sprite_button.c
#
from pyray import *

NUM_FRAMES = 3  # Number of frames (rectangles) for the button sprite texture

# Initialization
# --------------------------------------------------------------------------------------
screenWidth = 800
screenHeight = 450

init_window(screenWidth, screenHeight, "raylib [textures] example - sprite button")

init_audio_device()  # Initialize audio device

fxButton = load_sound("buttonfx.wav")  # Load button sound
button = load_texture("button.png")  # Load button texture

# Define frame rectangle for drawing
frameHeight = button.height / NUM_FRAMES
sourceRec = Rectangle(0, 0, button.width, frameHeight)

# Define button bounds on screen
btnBounds = Rectangle(screenWidth / 2.0 - button.width / 2.0, screenHeight / 2.0 - button.height / NUM_FRAMES / 2.0,
                      button.width, frameHeight)

btnState = 0  # Button state: 0-NORMAL, 1-MOUSE_HOVER, 2-PRESSED
btnAction = False  # Button action should be activated

mousePoint = Vector2(0.0, 0.0)

set_target_fps(60)
# --------------------------------------------------------------------------------------

# Main game loop
while not window_should_close():  # Detect window close button or ESC key
    # Update
    # ----------------------------------------------------------------------------------
    mousePoint = get_mouse_position()
    btnAction = False

    # Check button state
    if check_collision_point_rec(mousePoint, btnBounds):
        if is_mouse_button_down(MouseButton.MOUSE_BUTTON_LEFT):
            btnState = 2
        else:
            btnState = 1

        if is_mouse_button_released(MouseButton.MOUSE_BUTTON_LEFT):
            btnAction = True
    else:
        btnState = 0

    if btnAction:
        play_sound(fxButton)
        # TODO: Any desired action

    # Calculate button frame rectangle to draw depending on button state
    sourceRec.y = btnState * frameHeight
    # ----------------------------------------------------------------------------------

    # Draw
    # ----------------------------------------------------------------------------------
    begin_drawing()

    clear_background(RAYWHITE)

    draw_texture_rec(button, sourceRec, Vector2(btnBounds.x, btnBounds.y), WHITE)  # Draw button frame

    end_drawing()
    # ----------------------------------------------------------------------------------

# De-Initialization
# --------------------------------------------------------------------------------------
unload_texture(button)  # Unload button texture
unload_sound(fxButton)  # Unload sound

close_audio_device()  # Close audio device

close_window()  # Close window and OpenGL context
# --------------------------------------------------------------------------------------
