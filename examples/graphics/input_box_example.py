from pyray import *

MAX_INPUT_CHARS = 9


# Check if any key is pressed
# NOTE: We limit keys check to keys between 32 (KEY_SPACE) and 126
def IsAnyKeyPressed():
    keyPressed = False
    _key = get_key_pressed()

    if (_key >= 32) and (_key <= 126):
        keyPressed = True

    return keyPressed


# ------------------------------------------------------------------------------------
# Program main entry point
# ------------------------------------------------------------------------------------

# Initialization
# --------------------------------------------------------------------------------------
screenWidth = 800
screenHeight = 450

init_window(screenWidth, screenHeight, "raylib [text] example - input box")

name = ""
letterCount = 0

textBox = Rectangle(screenWidth / 2.0 - 100, 180, 225, 50)
mouseOnText = False

framesCounter = 0

set_target_fps(30)
# --------------------------------------------------------------------------------------

# Main game loop
while not window_should_close():
    # Update
    # ----------------------------------------------------------------------------------
    if check_collision_point_rec(get_mouse_position(), textBox):
        mouseOnText = True
    else:
        mouseOnText = False

    if mouseOnText:
        # Set the window's cursor to the I-Beam
        set_mouse_cursor(MouseCursor.MOUSE_CURSOR_IBEAM)

        # Get char pressed (unicode character) on the queue
        key = get_char_pressed()

        # Check if more characters have been pressed on the same frame
        while key > 0:
            # NOTE: Only allow keys in range [32..125]
            if (key >= 32) and (key <= 125) and (letterCount < MAX_INPUT_CHARS):
                # name[letterCount] = key
                name += chr(key)
                # name[letterCount + 1] = '\0'  # Add null terminator at the end of the string.
                letterCount += 1
            key = get_char_pressed()  # Check next character in the queue

        if is_key_pressed(KeyboardKey.KEY_BACKSPACE):
            name = name[:-1]
            letterCount -= 1
            if letterCount < 0:
                letterCount = 0
            # name[letterCount] = '\0'

    else:
        set_mouse_cursor(MouseCursor.MOUSE_CURSOR_DEFAULT)

    if mouseOnText:
        framesCounter = + 1
    else:
        framesCounter = 0
    # ----------------------------------------------------------------------------------

    # Draw
    # ----------------------------------------------------------------------------------
    begin_drawing()

    clear_background(RAYWHITE)

    draw_text("PLACE MOUSE OVER INPUT BOX!", 240, 140, 20, GRAY)

    draw_rectangle_rec(textBox, LIGHTGRAY)
    if mouseOnText:
        draw_rectangle_lines(int(textBox.x), int(textBox.y), int(textBox.width), int(textBox.height), RED)
    else:
        draw_rectangle_lines(int(textBox.x), int(textBox.y), int(textBox.width), int(textBox.height), DARKGRAY)

    draw_text(name, int(textBox.x) + 5, int(textBox.y) + 8, 40, MAROON)

    draw_text(f"INPUT CHARS: {letterCount}/{MAX_INPUT_CHARS}", 315, 250, 20, DARKGRAY)

    if mouseOnText:
        if letterCount < MAX_INPUT_CHARS:
            # Draw blinking underscore char
            if ((framesCounter / 20) % 2) == 0:
                draw_text("_", int(textBox.x) + 8 + measure_text(name, 40), int(textBox.y) + 12, 40, MAROON)
        else:
            draw_text("Press BACKSPACE to delete chars...", 230, 300, 20, GRAY)

    end_drawing()
    # ----------------------------------------------------------------------------------

# De-Initialization
# --------------------------------------------------------------------------------------
close_window()
# --------------------------------------------------------------------------------------
