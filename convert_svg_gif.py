import os
import subprocess

IMAGEMAGICK_DIR = "C:/Program Files/ImageMagick-7.1.1-Q16-HDRI/"

VISEME_LOCATION_DIR = "resources/viseme"


# To convert svg to gif:
# convert -density 1200 -resize 200x200 source.svg target.png
class SvgGifConverter:

    @staticmethod
    def convert_svg_gif(svg_filepath):
        os.chdir(VISEME_LOCATION_DIR)
        convert_command = ([IMAGEMAGICK_DIR + "convert", "-density", "1200", "-resize", "200x200"]
                           + [svg_filepath, svg_filepath.replace(".svg", ".png")])
        subprocess.call(convert_command)


if __name__ == "__main__":
    SvgGifConverter.convert_svg_gif("o.svg")
