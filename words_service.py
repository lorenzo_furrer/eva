from peewee import *
import os
from get_google_pronounciation import GooglePronounciationFetcher

db = SqliteDatabase(os.path.abspath('resources/db/words.db'))


class Word(Model):
    name = CharField()
    phonetic = CharField()
    viseme_sequence = CharField()
    duration_sequence = CharField()

    class Meta:
        database = db


class WordsService:

    # Gets word from local DB, if the word isn't there it stores it
    @staticmethod
    def get_word(word):
        word = word.lower()
        db.connect()
        try:
            word_model = Word.select().where(Word.name == word).get()
        except DoesNotExist:
            print(f"Word '{word}' does not exists in DB. Creating...")
            # phonetic = DictionaryApi.get_word_phonetic(word)
            google_pronounciation = GooglePronounciationFetcher.get_pronunciation_visemes(word)
            viseme_sequence = ','.join(list(map(lambda e: e[0], google_pronounciation)))
            duration_sequence = ','.join(list(map(lambda e: str(e[1]), google_pronounciation)))
            word_model = Word.create(name=word, phonetic="",
                                     viseme_sequence=viseme_sequence,
                                     duration_sequence=duration_sequence)
        db.close()
        visemes = word_model.viseme_sequence.split(',')
        durations = list(map(lambda e: int(e), word_model.duration_sequence.split(',')))
        result = []
        for i, viseme in enumerate(visemes):
            result.append([viseme, durations[i]])
        return result


def main():
    # # create database
    # db.connect()
    # db.create_tables([Word])
    # db.close()

    # # insert word
    # db.connect()
    # Word.create(name="dog", phonetic="phonetic example",
    #             viseme_sequence="sil,a_aa_uh,n_k_g_ng_y_r,n_k_g_ng_y_r,ax_i_ii,sil",
    #             duration_sequence="10,100,115,105,120,200")
    # db.close()

    # # retrieve word
    # db.connect()
    # word_model = Word.select().where(Word.name == "dog").get()
    # print(word_model.viseme_sequence)
    # print(word_model.duration_sequence)
    # db.close()

    # retrieve all words
    db.connect()
    words_model = list(Word.select().dicts())
    for word in words_model:
        print(f"{word['name']}: {word['viseme_sequence']}")
    db.close()

    # # Remove word
    # db.connect()
    # delete_query = Word.delete().where(Word.name == "are")
    # delete_query.execute()
    # db.close()


if __name__ == "__main__":
    main()
