
import subprocess

VISEME_LOCATION_DIR = "resources/viseme"


class GifGenerator:
    # Combines the selected images into a gif
    @staticmethod
    def generate_gif(frame_list):
        # os.chdir(VISEME_LOCATION_DIR)
        convert_command = ["convert", "-delay", "20", "-loop", "0"] + frame_list + ["animated.gif"]
        subprocess.call(convert_command)


if __name__ == "__main__":
    GifGenerator.generate_gif(["sil.png", "a_aa_uh.png", "n_k_g_ng_y_r.png", "n_k_g_ng_y_r.png", "ax_i_ii.png", "sil.png"])
