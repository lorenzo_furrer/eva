import logging
import speech_recognition as sr  # Speech to Text

logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(name)s] [%(levelname)s] %(message)s')


def main():
    r = sr.Recognizer()  # Record Audio
    logging.info("Energy threshold is: " + str(r.energy_threshold))

    with sr.Microphone() as source:
        logging.info("Say something!")
        audio = r.listen(source)
    # Speech recognition using Google Speech Recognition
    try:
        # for testing purposes, we're just using the default API key
        # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
        # instead of `r.recognize_google(audio)`
        spoken_words = r.recognize_google(audio)
        logging.info("You said: " + spoken_words)
        # subprocess.Popen(["python", LAUNCH_COMMAND_DIR + "launch_command.py", spokenWords])
        # LaunchCommand.launch_command(spokenWords)
        return spoken_words
    except sr.UnknownValueError:
        logging.warning("Google Speech Recognition could not understand audio")
    except sr.RequestError as e:
        logging.error("Could not request results from Google Speech Recognition service; {0}".format(e))
    except Exception as e:
        logging.error("Generic exception thrown; {0}".format(e))


if __name__ == "__main__":
    main()
