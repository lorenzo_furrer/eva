import requests
from bs4 import BeautifulSoup


class GooglePronounciationFetcher:

    @staticmethod
    def get_pronunciation_visemes(word):
        query = '+'.join(word.split())
        search_url = f"https://www.google.com/search?q=how+to+pronounce+{query}"

        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36",
            "Accept-Language": "en-US"
        }
        response = requests.get(search_url, headers=headers)

        if response.status_code != 200:
            return "Failed to fetch data from Google."

        # print(response.content)

        soup = BeautifulSoup(response.content, 'html.parser')

        viseme_div = soup.find("div", {"data-attrid": "Viseme"})
        if not viseme_div:
            return [["sil", 10]]

        result = []
        g_img_elements = viseme_div.find_all("g-img")
        for g_img in g_img_elements:
            if g_img.has_attr("data-viseme-duration"):
                duration = g_img["data-viseme-duration"]

                img_element = g_img.find("img")
                if img_element and img_element.has_attr("data-src"):
                    img_src = img_element["data-src"]
                    img_src = img_src.split('/')[-1].replace('.svg', '')
                    print(f"Image Source: {img_src}, Viseme Duration: {duration}")
                    result.append([img_src, round(int(float(duration)))])
        return result


def main():
    word_to_search = "anchor"
    result = GooglePronounciationFetcher.get_pronunciation_visemes(word_to_search)
    print(result)


if __name__ == "__main__":
    main()
