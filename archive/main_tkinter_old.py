#! python3
# Requires PyAudio and PySpeech.
import logging
import tkinter  # GUI
import subprocess
from generate_gif import GifGenerator

logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(name)s] [%(levelname)s] %(message)s')


class MasterGui:
    LISTEN_COMMAND_DIR = "/Users/lorefurr/perp/eva-workspace/eva/"
    SPEAK_COMMAND_DIR = "/Users/lorefurr/perp/eva-workspace/eva/"
    SPEAK_START_DELAY = 1000  # milliseconds
    default_image = None

    def __init__(self):
        self.root = tkinter.Tk()
        self.default_image = tkinter.PhotoImage(file="resources/viseme/sil.png")
        self.label = tkinter.Label(self.root, image=self.default_image)
        self.label.pack()
        self.listen_button = tkinter.Button(self.root, command=self.listen_button_onclick, text="Speak!")
        self.listen_button.pack()
        self.root.mainloop()

    def listen_button_onclick(self):
        logging.info("Clicked!")
        # spoken_words = self.listen()
        viseme_array = self.get_visemes(None)
        GifGenerator.generate_gif(viseme_array)
        self.speak("anchor anchorage")
        self.animate_gif(viseme_array)
        logging.info("Finished!")

    def listen(self):
        return subprocess.check_output(["python3", self.LISTEN_COMMAND_DIR + "listen.py"])

    def speak(self, sentence):
        subprocess.Popen(["python3", self.SPEAK_COMMAND_DIR + "speaker_pyttsx3.py", sentence])

    def get_visemes(self, spoken_words):
        return ["sil.png",
                "a_aa_uh.png", "n_k_g_ng_y_r.png", "n_k_g_ng_y_r.png", "ax_i_ii.png",
                "sil.png",
                "a_aa_uh.png", "n_k_g_ng_y_r.png", "n_k_g_ng_y_r.png", "ax_i_ii.png", "n_k_g_ng_y_r.png", "ax_i_ii.png", "sh_zh_ch_jh.png",
                "sil.png"]

    def animate_gif(self, viseme_array):
        num_frames = len(viseme_array)
        frames = [
            tkinter.PhotoImage(file="/Users/lorefurr/perp/eva-workspace/eva/resources/viseme/animated.gif",
                               format="gif -index %i" % (i)) for i in range(num_frames)]

        def update_frame(frame_index):
            frame = frames[frame_index]
            self.label.configure(image=frame)
            frame_index += 1
            if frame_index < num_frames:
                self.root.after(100, update_frame, frame_index)
            else:
                self.label.configure(image=self.default_image)
        self.root.after(self.SPEAK_START_DELAY, update_frame, 0)


if __name__ == "__main__":
    MasterGui()
