import logging
import os
import pyttsx3  # Text to Speech
from threading import Thread


class Speaker:

    def __init__(self):
        self.rate = 210
        self.sentence_queue = []
        self.output_file_index = 0

    def _initialize_engine(self, on_start_callback=None, on_end_callback=None):
        self.engine = pyttsx3.init("nsss")
        # voices = self.engine.getProperty('voices')
        self.engine.setProperty('voice', "com.apple.speech.synthesis.voice.samantha")
        self.engine.setProperty('rate', self.rate)
        if on_start_callback is not None:
            self.engine.connect('started-utterance', on_start_callback)
        if on_end_callback is not None:
            self.engine.connect('finished-utterance', on_end_callback)

    def speak(self, sentence):
        self.sentence_queue.append(sentence)

    def speak_now(self, sentence, on_start_callback=None, on_end_callback=None):
        speak_now_thread = Thread(target=self.speak_now_runnable, args=(sentence, on_start_callback, on_end_callback,))
        speak_now_thread.start()

    def speak_now_runnable(self, sentence, on_start_callback=None, on_end_callback=None):
        # self._initialize_engine(on_start_callback)
        # self.engine.say(sentence)
        # self.engine.runAndWait()  # this call is blocking
        logging.info(f"Say:- {sentence}")
        if on_start_callback is not None:
            on_start_callback()
        os.system(f'say -v Samantha -r {self.rate} "{sentence}"')
        logging.info("Stopped speaking")
        if on_end_callback is not None:
            on_end_callback()


def main():
    logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(name)s] [%(levelname)s] %(message)s')
    speaker = Speaker()
    speaker.speak_now("anchor")
    speaker.speak_now("Hi, how do you do?")


if __name__ == "__main__":
    main()
