#! python3
# Requires PyAudio and PySpeech.
import logging
import random

from pyray import *
from speaker_pyttsx3 import Speaker
from words_service import WordsService
from timer import Timer

logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(name)s] [%(levelname)s] %(message)s')

SILENCE_VISEME = "sil"

SENTENCE = "The quick brown fox jumped over the lazy dog"
# SENTENCE = "anchor how do you do"

TEXTURE_EXPRESSIONS_X_COORD = [
    14,  # sil
    272,  # tongue
    458,  # eyes left
    515,  # eyes right
    571,  # eyes closed
]
TEXTURE_EXPRESSION_DRAW_HEIGHT = 35

TEXTURE_VISEME_SCALE = 5
TEXTURE_VISEME_DRAW_WIDTH = 50
TEXTURE_VISEME_DRAW_HEIGHT = 51
TEXTURE_VISEME_DRAW_Y_COORDINATE = 15
TEXTURE_VISEME_DRAW_X_COORDINATES = {
    0: 14,
    1: 80,
    2: 146,
    3: 209,
    4: 272,
    5: 334,
}
VISEME_TEXTURE_MAP = {
    "sil": 0,
    "a_aa_uh": 1,
    "ax_i_ii": 2,
    "n_k_g_ng_y_r": 3,
    "n_k_g_ng_y_r-w_oo_uu_o_u": 3,
    "sh_zh_ch_jh": 4,

    "p_b_m": 3,
    "t_d_s_z": 3,
    "t_d_s_z--w_oo_uu_o_u": 3,
    "f_v": 3,
    "th_dh": 4,
    "l": 4,
    "w_oo_uu_u": 5,
    "e": 2,
    "ei": 1,
    "o": 5,
}


def load_text_to_speak(sentence):
    sentence_visemes_textures_coords = []
    for word in sentence.split(" "):
        viseme_duration_map = WordsService.get_word(word)
        sentence_visemes_textures_coords.extend(
            list(
                map(lambda e: [
                    TEXTURE_VISEME_DRAW_X_COORDINATES[VISEME_TEXTURE_MAP.get(e[0], 0)],
                    e[1]
                ], viseme_duration_map))
        )
    # print(sentence_visemes_textures_coords)
    return sentence_visemes_textures_coords


def initialize_input_text_box():
    return Rectangle(50, get_screen_height() - 100, 550, 75)


def initialize_speak_button():
    return Rectangle(get_screen_width() - 150, get_screen_height() - 100, 120, 75)


def draw_input_text_box(rec, text, _font_size):
    draw_rectangle_rec(rec, BLACK)
    draw_rectangle_lines(int(rec.x), int(rec.y), int(rec.width),
                         int(rec.height), RAYWHITE)
    draw_text(text, int(rec.x) + 5, int(rec.y) + 8, _font_size, RAYWHITE)


def draw_speak_button(rec, is_mouse_over, _font_size):
    draw_rectangle_rec(rec, RAYWHITE if is_mouse_over else BLACK)
    draw_rectangle_lines(int(rec.x), int(rec.y), int(rec.width),
                         int(rec.height), RAYWHITE)
    draw_text("SPEAK", int(rec.x + rec.width/2 - 40),
              int(rec.y + rec.height/2 - _font_size/2),
              _font_size,
              BLACK if is_mouse_over else RAYWHITE)


def start_animation():
    timer.start_timer(100 / 1000)


def get_expreesion_delay():
    return random.randint(1000, 2000) / 1000


def replace_last_space_with_newline(sentence):
    last_space_index = sentence.rfind(' ')
    if last_space_index != -1:
        return sentence[:last_space_index] + '\n' + sentence[last_space_index + 1:]
    return sentence


if __name__ == "__main__":
    init_window(800, 450, "Hello")

    font: Font = get_font_default()
    font_size = 20
    scaleFactor: float = font_size / font.baseSize  # Character rectangle scaling factor

    input_sentence = ""

    input_text_box = initialize_input_text_box()
    speak_button_rect = initialize_speak_button()

    all_viseme_tex = load_texture(f"resources/python-eva-all-poses.png")
    sourceRec = Rectangle(TEXTURE_VISEME_DRAW_X_COORDINATES[0], TEXTURE_VISEME_DRAW_Y_COORDINATE,
                          TEXTURE_VISEME_DRAW_WIDTH, TEXTURE_VISEME_DRAW_HEIGHT)

    expression_source_rec = Rectangle(TEXTURE_EXPRESSIONS_X_COORD[0], TEXTURE_VISEME_DRAW_Y_COORDINATE,
                                      TEXTURE_VISEME_DRAW_WIDTH, TEXTURE_VISEME_DRAW_HEIGHT)

    viseme_position = Vector2(get_screen_width() / 2 - TEXTURE_VISEME_DRAW_WIDTH * TEXTURE_VISEME_SCALE / 2,
                              get_screen_height() / 2 - TEXTURE_VISEME_DRAW_HEIGHT * TEXTURE_VISEME_SCALE / 2 - 20)

    timer = Timer()
    viseme_draw_index = 0
    sentence_visemes_tex_coords = [[TEXTURE_VISEME_DRAW_X_COORDINATES[VISEME_TEXTURE_MAP[SILENCE_VISEME]], 0]]

    expression_timer = Timer()
    expression_timer.start_timer(get_expreesion_delay())

    while not window_should_close():
        # Input
        mouse_position = get_mouse_position()

        if check_collision_point_rec(mouse_position, input_text_box):
            mouseOnText = True
        else:
            mouseOnText = False

        if check_collision_point_rec(mouse_position, speak_button_rect):
            mouse_over_speak_button = True
        else:
            mouse_over_speak_button = False

        if mouseOnText:
            key = get_char_pressed()
            while key > 0:
                # NOTE: Only allow keys in range [32..125]
                if (key >= 32) and (key <= 125):
                    input_sentence += chr(key)

                last_line_width = 0
                for character in input_sentence.split('\n')[-1]:
                    if character != '\n':
                        codepoint: int = get_codepoint(character, 0)
                        index: int = get_glyph_index(font, codepoint)
                        glyphWidth = font.recs[index].width * scaleFactor if (font.glyphs[index].advanceX == 0) \
                            else font.glyphs[index].advanceX * scaleFactor
                        last_line_width += glyphWidth + 2.0  # 2.0 is spacing
                print(last_line_width)
                if last_line_width > input_text_box.width:
                    input_sentence = replace_last_space_with_newline(input_sentence)
                key = get_char_pressed()  # Check next character in the queue

            if is_key_pressed(KeyboardKey.KEY_BACKSPACE):
                if len(input_sentence) > 0:
                    input_sentence = input_sentence[:-1]

        if check_collision_point_rec(mouse_position, speak_button_rect):
            if is_mouse_button_released(MouseButton.MOUSE_BUTTON_LEFT):
                if input_sentence != "":
                    sentence_visemes_tex_coords = load_text_to_speak(input_sentence)
                    # set expression draw height to roughly half image so not to draw over the mouth movements
                    expression_source_rec.height = TEXTURE_EXPRESSION_DRAW_HEIGHT
                    s = Speaker()
                    s.speak_now(input_sentence, on_start_callback=start_animation)

        # Update
        if timer.is_done():
            viseme_draw_index += 1
            if viseme_draw_index >= len(sentence_visemes_tex_coords):
                viseme_draw_index = 0
                sentence_visemes_tex_coords = [
                    [TEXTURE_VISEME_DRAW_X_COORDINATES[VISEME_TEXTURE_MAP[SILENCE_VISEME]], 0]]
                timer.stop_timer()
                expression_source_rec.height = TEXTURE_VISEME_DRAW_HEIGHT
            else:
                timer.start_timer(sentence_visemes_tex_coords[viseme_draw_index][1] / 1000)
            sourceRec.x = sentence_visemes_tex_coords[viseme_draw_index][0]

        if expression_timer.is_done():
            expression_source_rec.x = TEXTURE_EXPRESSIONS_X_COORD[random.randint(0, len(TEXTURE_EXPRESSIONS_X_COORD) - 1)]
            expression_timer.start_timer(get_expreesion_delay())

        # Draw
        begin_drawing()
        clear_background(BLACK)

        draw_input_text_box(input_text_box, input_sentence, font_size)
        draw_speak_button(speak_button_rect, mouse_over_speak_button, font_size + 4)

        # Draw lower face: pronounciation
        draw_texture_pro(all_viseme_tex, sourceRec,
                         Rectangle(viseme_position.x, viseme_position.y,
                                   sourceRec.width * TEXTURE_VISEME_SCALE, sourceRec.height * TEXTURE_VISEME_SCALE),
                         Vector2(0, 0), 0.0, WHITE)

        # Draw upper face: expression
        draw_texture_pro(all_viseme_tex, expression_source_rec,
                         Rectangle(viseme_position.x, viseme_position.y,
                                   expression_source_rec.width * TEXTURE_VISEME_SCALE,
                                   expression_source_rec.height * TEXTURE_VISEME_SCALE),
                         Vector2(0, 0), 0.0, WHITE)
        end_drawing()
    close_window()
