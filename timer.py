from pyray import *


class Timer:

    def __init__(self):
        self.is_started = False
        self.start_time = 0
        self.life_time = 0

    def start_timer(self, life_time):
        self.is_started = True
        self.start_time = get_time()
        self.life_time = life_time

    def stop_timer(self):
        self.is_started = False

    def reset_timer(self):
        self.is_started = True
        self.start_time = get_time()

    def is_done(self):
        return self.is_started and self.get_elapsed() >= self.life_time

    def get_elapsed(self):
        return get_time() - self.start_time
