visemes = [
    "sil",
    "a_aa_uh",
    "ax_i_ii",
    "n_k_g_ng_y_r",
    "n_k_g_ng_y_r-w_oo_uu_o_u",
    "sh_zh_ch_jh",

    "p_b_m",
    "t_d_s_z",
    "t_d_s_z--w_oo_uu_o_u",
    "f_v",
    "th_dh",
    "l",
    "w_oo_uu_u",
    "e",
    "ei",
    "o",
]

phoeneme_map = {
    "/": "sil",
    "ˈ": "sil",
    ".": "sil",

    "æ": "a_aa_uh",
    "ŋ": "n_k_g_ng_y_r",
    "k": "n_k_g_ng_y_r",
    "ə": "ax_i_ii",


    "p": "p_b_m",
    "b": "p_b_m",
    "t": "t_d_s_z",
    "d": "t_d_s_z",
    "ɡ": "n_k_g_ng_y_r",
    "m": "p_b_m",
    "n": "n_k_g_ng_y_r",
    "f": "f_v",
    "v": "f_v",
    "s": "t_d_s_z",
    "z": "t_d_s_z",
    "θ": "th_dh",
    "ð": "th_dh",
    "ʃ": "sh_zh_ch_jh",
    "ʒ": "sh_zh_ch_jh",
    "h": "a_aa_uh",
    "l": "l",
    "ɹ": "n_k_g_ng_y_r",
    "ʧ": "sh_zh_ch_jh",
    "ʤ": "sh_zh_ch_jh",
    "j": "n_k_g_ng_y_r",
    "w": "w_oo_uu_u",
    "ɚ": "ax_i_ii",
    "ɛ": "e",
    "ɪ": "ax_i_ii",
    "iː": "ax_i_ii",
    "ʊ": "w_oo_uu_u",
    "uː": "w_oo_uu_u",
    "ʌ": "a_aa_uh",
    "a": "a_aa_uh",
    "e": "ei",
    "ɔ": "o",
    "o": "o",
    "ɑː": "a_aa_uh",

    "ɒ": "a_aa_uh",
    "i": "ax_i_ii",
    "u": "w_oo_uu_u",
    "ɑ": "a_aa_uh",
}

anchor = "/ˈæŋ.kə/"
popular = "/ˈpɒpjʊlə/"
bubble = "/ˈbʌb.əl/"
tinker = "/ˈtɪŋkə(ɹ)/"
dog = "/dɑɡ/"
