#! python3

import sys
import re
import subprocess
from eva.open_pages import OpenPages

commands = {
    "open_pages.py": ["open"],
}
programs = {
    "newrelic": ["new relic", "newrelic"],
    "log": ["log", "logs"]
}
environments = {
    "superdrug": ["superdrug"],
    "elab": ["elab"],
    "mat": ["marionnaud austria", "mat", "m a t"]
}

open_pages = OpenPages()


def find_command(spoken_commands):
    return find_statement(spoken_commands, commands)


def find_program(spoken_commands):
    return find_statement(spoken_commands, programs)


def find_environment(spoken_commands):
    return find_statement(spoken_commands, environments)


def analyze_command(spoken_commands):
    command = find_command(spoken_commands)
    print("Found command: " + str(command))
    program = find_program(spoken_commands)
    print("Found program: " + str(program))
    environment = find_environment(spoken_commands)
    print("Found environment: " + str(environment))
    parsed_command = []
    if command is None:
        print("No command was understood.")
        return None
    parsed_command += [command]
    if program is not None:
        parsed_command += [program]
    if environment is not None:
        parsed_command += [environment]
    return parsed_command


def find_statement(spoken_commands, statement):
    for k in statement.keys():
        for activator in statement[k]:
            command_regex = re.compile(activator, re.IGNORECASE)
            mo = command_regex.search(spoken_commands)
            if mo is not None:
                print("Found pattern:" + mo.group())
                return k
    return None


class LaunchCommand:

    @staticmethod
    def launch_command(spoken_commands):
        command_parameters_list = analyze_command(spoken_commands)
        if command_parameters_list is None:
            return
        print("Launching command with parameters: " + str(command_parameters_list[1]))
        open_pages.open_page(command_parameters_list[1])
