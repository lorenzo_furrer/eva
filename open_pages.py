import os
import sys
import subprocess
import datetime
from selenium import webdriver

chromedriver = "/Users/lorefurr/p/scripts/chromedriver"
os.environ["webdriver.chrome.driver"] = chromedriver
destinations = {
    "newrelic": {
        "program": "browser",
        "url": "https://login.newrelic.com/login",
        "superdrug": "https://rpm.newrelic.com/accounts/717846/applications",
        "elab": "https://rpm.newrelic.com/accounts/717847/applications"
    },
    "log": {
        "program": "baretail",
        "path": "C:\\Users\\Lfurrer\\Desktop\\baretail.exe",
        "superdrug": "C:\\p\\superdrug\\hybris_SPDG_DEV_1.38\\log\\tomcat\\console-{0}.log".format(
            datetime.datetime.now().strftime("%Y%m%d")),
        "mat": "C:\\p\\marionnaud\\hybris_mat_trunk\\log\\tomcat\\console-{0}.log".format(
            datetime.datetime.now().strftime("%Y%m%d"))
    }
}


class OpenPages:

    @staticmethod
    def normalize_args(args):
        return "".join(args[1:-1]).lower()

    def open_page(self, params):
        destination = destinations[params]

        if destination["program"] == "browser":
            driver = webdriver.Chrome(chromedriver)
            driver.get(destination["url"])
            email_input = driver.find_element_by_css_selector("#login_email")
            email_input.send_keys("lorenzo.furrer@pearson.com")
            # passwordInput = driver.find_element_by_css_selector("#login_password")
            # passwordInput.send_keys("")
            submit_input = driver.find_element_by_css_selector("#login_submit")
            submit_input.submit()

            environment = sys.argv[-1].lower()
            driver.get(destination[environment])
        else:
            environment = sys.argv[-1].lower()
            subprocess.Popen([destination["path"], destination[environment]])
