import requests

# https://dictionaryapi.dev/
# https://api.dictionaryapi.dev/api/v2/entries/en/<word>


class DictionaryApi:

    @staticmethod
    def get_word_phonetic(word):
        response = requests.get(f"https://api.dictionaryapi.dev/api/v2/entries/en/{word}")
        res_json = response.json()
        print(res_json)
        # print(json.dumps(response.json(), indent=4))
        word_definition = res_json[0]
        if "phonetic" in word_definition:
            return res_json[0]["phonetic"]
        return ""


if __name__ == "__main__":
    print(DictionaryApi.get_word_phonetic("dog"))
