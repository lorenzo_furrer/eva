import pyautogui

pyautogui.PAUSE = 1.5
pyautogui.FAILSAFE = True

print("Press Ctrl-C to quit")

resX, resY = pyautogui.size()
print("Screen resolution: " + str(resX) + ", " + str(resY))

try:
    while True:
        x, y = pyautogui.position()
        formattedPosition = "X: " + str(x).rjust(10) + ", Y: " + str(y).rjust(10)
        #pixelColor = pyautogui.screenshot().getpixel((x, y))
        #formattedPosition += ", RGB: ("+str(pixelColor[0]).rjust(3)+","+str(pixelColor[1]).rjust(3)+","+str(pixelColor[2]).rjust(3)+")"
        print(formattedPosition, end="")
        print("\b" * len(formattedPosition), end="", flush=True)
except KeyboardInterrupt:
    print("\nDone!")
